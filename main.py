# -*- coding: utf-8 -*-
import nltk
import keyboard
import sys
import os
import re

check = False # Выводить ли информацию при запуске программы
version = "0.0.1" # Версия программы
# TODO: дописать другие символы
# НЕЛЬЗЯ УСТАНАВЛИВАТЬ "_", ибо это соединение между словами
stop_symbols = [",", ".", "-", "?", "!", "—", "«", "»", "(", ")", "\"", "'", "&", "[", "]"] # Символы, которые удаляются из предложений
delimiter = ";"
mls = [] # Сформированные молинги для сохранения
max_words = 15 # Максимальное количество слов в предложении
trigger_sentence = ">." # Триггер для нового абзаца
trigger_stop_sentence = "<." # Триггер, чтобы не повышать номер предложения(используется, если предложение сложное)

# Install nltk:
# https://www.youtube.com/watch?v=68aHmFcO-W4
# Readme:
# https://habr.com/ru/company/Voximplant/blog/446738/

# •	source - источники, книжки.
# •	kb.txt – файл БЗ из молинг; УЖЕ ГОТОВАЯ.
# •	1.txt – файл словаря Терминов;
# •	2.txt – файл словаря Отношений
# •	3.txt – файл словаря Качественных признаков;
# •	4.txt – файл словаря Количественных признаков;
# •	5.txt – файл словаря Логических и Лингвистических связующих элементов;
# •	6.txt – файл картотеки Значений Терминов (словарь интерпретаций);

# Input
src_1st = "src/1.txt"
src_2nd = "src/2.txt"
src_3rd = "src/3.txt"
src_4th = "src/4.txt"
src_5th = "src/5.txt"
src_6th = "src/6.txt"

dict_files_in = [src_1st, src_2nd, src_3rd, src_4th, src_5th, src_6th]

src_inp = "src/in.txt"
src_inp_number_src = 1 # Номер источника
src_inp_default_F = 0.5
src_src = "src/source.txt"

# Work
dict_words = [[]] # Это связано с тем, чтобы словарь под номер 1 распологался под номером 1

# Output
dst_1st = "dst/1.txt"
dst_2nd = "dst/2.txt"
dst_3rd = "dst/3.txt"
dst_4th = "dst/4.txt"
dst_5th = "dst/5.txt"
dst_6th = "dst/6.txt"
dict_files_out = [dst_1st, dst_2nd, dst_3rd, dst_4th, dst_5th, dst_6th]

dst_out = "dst/KB.txt"
dst_src = "dst/source.txt"

# Molinga
class Molinga(object):
    """docstring"""
    # Пример:
    # 3.1.3.3.3.1;; Машина_вывода включает два компонента.; 1, 2, 4, 1; 0,4; ;
    # В них идентификатор 3.1.3.3.3.1 означает: 
    # 3 - номер источника, 
    # 1 – номер главы, 
    # 3 – номер параграфа, 
    # 3 – номер подпараграфа, 
    # 3 – порядковый номер абзаца в нем, 
    # 1 – номер предложения в абзаце
    def __init__(self, src, chapter, paragraph, subparagraph, indent, sentence, P, Z, K, F, N):
        self.src = src # Номер источника
        self.chapter = chapter # Номер главы
        self.paragraph = paragraph # Номер параграфа
        self.subparagraph = subparagraph # Номер подпараграфа
        self.indent = indent # Порядковый номер абзаца в нем
        self.sentence = sentence # Номер предложения в абзаце
        self.P = P # Условие применимости ядра молинги
        self.Z = Z # Моделируемое простое предложение
        self.K = K # Кодовая последовательность номеров словарей
        self.F = F # Фактор уверенности
        self.N = N # Постусловие молинги

    # Преобразование в строку, можно обратиться к экземпляру класса как print(X) или str(X)
    def __str__(self):
        # Преобразование кортежа в строку
        sarr = [str(a) for a in self.K]
        K_render = (", " . join(sarr))
        # Готовая молинга для печати или записи
        # TODO: фактор уверенности через запятую бы
        # https://ru.stackoverflow.com/questions/1131165
        return "%i.%i.%i.%i.%i.%i;%s; %s; %s; %.1f; %s;" % (self.src, 
                                                            self.chapter, 
                                                            self.paragraph, 
                                                            self.subparagraph, 
                                                            self.indent, 
                                                            self.sentence, 
                                                            self.P, 
                                                            self.Z, 
                                                            K_render, 
                                                            self.F, 
                                                            self.N ) 

def print_info():
    if check:
        print ("Molinga Converter")
        print ("Author: Ermeykin \"Mist /b/\" Sergey")
        print ("License: GPL v2")
        print ("Version: " + str (version))

# Функция считывает файл и словарь в виде list
def read_dict(file):
    dict_words = []
    with open(file, mode="r", encoding="utf-8") as file:
        for words in file:
            words = words[:-1]
            # Разбиение на синонимы
            l = words.split(', ')
            # Первому слову буквы понижаем
            l[0] = l[0].lower()
            # Добавляем список в словарь
            dict_words.append(l)
    return dict_words

# Функция сохраняет словарь в файл
def save_dict(dict_words, file):
    with open(file, mode="w", encoding="utf-8") as file:
        for dict_word in dict_words:
            file.write(dict_word + "\n")

# Функция считывает файл и отдает предложения
def read_inp(file):
    sentences = []
    with open(file, mode="r", encoding="utf-8") as file:
        for line in file:
            # Если новый абзац, то ставится триггер
            sentences.append(trigger_sentence)
            for sentence in nltk.sent_tokenize(line):
                sentences.append(sentence)
    return sentences

# Функция сохраняет kb в файл
def save_kb(mls, file):
    with open(file, mode="w", encoding="utf-8") as file:
        for ml in mls:
            file.write(str(ml) + "\n")

# Сохраняет всё за собой и выходит программы
def save_files():
    # Сохранение словарей в 1-6.txt
    # Удаление "нулевого" словаря для правильного смещения у dict_words
    # Словарь избавляется от дубликатов и сортируется
    for dict_word, dict_file in zip(dict_words[1:], dict_files_out):
        # List вида 
        # [["молоко", "молока", "молоку"], ["сын", "сына", "сыном"], ["небо", "неба", "небу"]]
        # Преобразуется в list вида
        # ["Молоко, молока, молоку", "Сын, сына, сыном","Небо, неба, небу"]
        # ["молоко", "молока", "молоку"] -> "Молоко, молока, молоку"
        dw_str_list = []
        for dw_list in dict_word:
            dw_str = ', '.join(dw_list)
            dw_str_list.append (dw_str.capitalize())
        save_dict(sorted(set(dw_str_list)), dict_file)

    # Сохранение КБ в kb.txt
    save_kb(mls, dst_out)
    
    # Выход
    # TODO: не работает
    sys.exit()

# Очистка экрана
def clear():
    os.system('cls' if os.name=='nt' else 'clear')

# Поиск слова в словаре, возвращает номер словаря
# Если слова нет в словаре, то добавляет его туда
def word_in_dict(word):
    # Преобразование слова к нижнему регистру
    word = word.lower()
    # Ищем слово в словаре
    word_in_dict = set()
    i = 0
    # Проход по словарям
    for dict_word in dict_words:
        # Проход по словам(строки текстового файла)
        for word_from_dict in dict_word:
            # Проверка есть ли слово в строке(синонимы)
            if word in word_from_dict:
                word_in_dict.add(i)
                continue
        i = i + 1
    # Решаем как поступать
    l = len(word_in_dict)
    if l == 1:
        print ("Слово \033[1m", word, "\033[0m найдено в одном из словарей.")
        return word_in_dict.pop()
    elif l == 0:
        print ("Слово \033[1m", word, "\033[0m не найдено в словарях.")
    else:
        print ("Слово \033[1m", word, "\033[0m найдено в нескольких словарях. Словари: ", str(word_in_dict))
    # Граница словарей, чтобы не вылезти за неё
    low, hi = 0, 8
    while True:
        number_dict = input('Введи номер словаря 1-6 (7 - сохранение, не забыть скопировать DST): : '.format_map(vars()))
        try:
            number_dict = int(number_dict)
        except ValueError:
            print('Попробуй снова.')
        else:
            if low < number_dict < hi:
                break
            print('Попробуй снова.')
    # TODO:
    # Если слова нет в словаре, то:
    # 1 - предложить ручную установку
    # 2 - предложить полуавтоматическую установку на основе запроса в словарь тырнетов и ручного подтверждения
    # 3 - предложить автоматическую установку на основе запросов в словарь тырнетов
    # Слово заносится в словарь, т.к. там его нету
    # TODO: лучше придумать отдельную функцию, которая помещает слово в словарь
    if (number_dict == 7):
        print ("Save begin")
        save_files()
    dict_words[number_dict].append([word])
    print ("<----- Слово выполнено.")
    return number_dict

if __name__ == "__main__":
    # Скачивание словаря, чтобы разделять предложения на слова
    nltk.download('punkt')
    # Чтобы экстренно выйти из программы и сохранить наработки, требуется нажать Ctrl + 3
    keyboard.add_hotkey('Ctrl + 3', save_files)

    # Объявление переменных для идентификации предложения
    chapter, paragraph, subparagraph, indent = 1, 0, 0, 1
    flag_up_sentence = False

    # Вывод информации о программе
    print_info()

    # Считывание словарей
    for dict_file in dict_files_in:
        dict_words.append(read_dict(dict_file))

    # Считывание предложений из src_inp
    sentences = read_inp(src_inp)
    # Работа с каждым предложением
    for sentence in sentences:
        # Если есть указатель, то он применяется
        # Получается номер главы, параграфа, подпараграфа и абзаца
        if re.match("\d+-\d+\-\d+-\d+.", sentence):
            chapter, paragraph, subparagraph, indent = [int(x) for x in re.split(r'-', sentence[:-1])]
            print (chapter, paragraph, subparagraph, indent)
            continue
        # Обнуление номера предложение, если новый абзац, и повышение номера абзаца
        if (trigger_sentence == sentence):
            number_sentence = 0
            indent = indent + 1
            continue

        # Номер предложения не повышается, если встретилась эта последовательность
        if (trigger_stop_sentence == sentence):
            flag_up_sentence = True
            continue
        if (flag_up_sentence):
            flag_up_sentence = False
        else:
            # Увеличение номера предложения
            number_sentence = number_sentence + 1

        # Разбивка на слова
        words = nltk.word_tokenize(sentence)
        for word in words:
            if word == delimiter:
                print ("ERROR: В предложении найден делитель \"", delimiter, "\". Это запрещено. Предложение: ", sentence)
                save_files()
        # Очистка предложения от стоп-символов (",", "." и т.д.)
        words_cleaning = words
        for stop_symbol in stop_symbols:
            words_cleaning = list(filter((stop_symbol).__ne__, words_cleaning))
        # Проверка предложения на максимальное количество слов и выход при положительном исходе
        if (len(words_cleaning) > max_words):
            print ("ERROR: В предложении слов больше допущенного. Предложение: ", sentence)
            i = 1
            for word in words_cleaning:
                print (str(i), ' --> ', word)
                i = i + 1
            save_files()
        # Работаем по словам предложения
        # Формируем кодовую последовательность для слов
        K = [] # K для ядра молинги
        for word in words_cleaning:
            clear()
            print(sentence)
            K.append(word_in_dict(word))
        # Преобразование предложения в молингу
        print ("Ориг. пред. : ", sentence)
        ml = Molinga(src_inp_number_src, chapter, paragraph, subparagraph, indent, number_sentence, "", sentence, K, src_inp_default_F, "")
        print ("Молинга: ", str(ml))
        mls.append(ml)
        print ("-----> Предложение выполнено.")
        # input("Press Enter key...")
        clear()

    # Сохранение и выход
    save_files()

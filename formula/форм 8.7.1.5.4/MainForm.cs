﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace форм_8._1._2._4
{
    /// <summary>
    /// Класс главной формы
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Конструктор главной формы
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }
        private int CX = 0;
        private double C0 = 0;
        private double varResult;
        private List<double> C = new List<double>();
        private List<double> X = new List<double>();



        private double VarResult { get => varResult; set => varResult = value; }

        private void textBoxM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsDigit(e.KeyChar)) && (e.KeyChar != ',') && (e.KeyChar != '.') && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void textBoxI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsDigit(e.KeyChar)) && (e.KeyChar != ',') && (e.KeyChar != '.') && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void textBoxT3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsDigit(e.KeyChar)) && (e.KeyChar != ',') && (e.KeyChar != '.') && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void textBoxT4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsDigit(e.KeyChar)) && (e.KeyChar != ',') && (e.KeyChar != '.') && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            textBoxCX.Text = CX.ToString();
            textBoxС0.Text = C0.ToString();
            buttonCommitResult.Enabled = false;
        }

        private void buttonCompute_Click(object sender, EventArgs e)
        {
            C.Clear();
            X.Clear();
            textBoxС0.Text = textBoxС0.Text.Replace(oldChar: '.', newChar: ',');
            CX = Convert.ToInt32(textBoxCX.Text);
            C0 = Convert.ToDouble(textBoxС0.Text);
            VarResult = C0;
            for (int i = 0; i < CX; i++)
            {
                CXForm cxForm = new CXForm();
                var result = cxForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    VarResult += cxForm.ReturnC * cxForm.ReturnX;
                }
                C.Add(cxForm.ReturnC);
                X.Add(cxForm.ReturnX);
            }
            textBoxResult.Text = VarResult.ToString();
            buttonCommitResult.Enabled = true;
        }

        private void buttonEraseVariables_Click(object sender, EventArgs e)
        {
            textBoxCX.Clear();
            textBoxС0.Clear();
        }

        private void buttonCommitResult_Click(object sender, EventArgs e)
        {
            String CXResult = "";
            for (int i = 0; i < C.Count; i++)
            {
                CXResult += $"C{i + 1} = {C[i]}; X{i + 1} = {X[i]}; ";
            }
            String cntnts = $"форм 8.7.1.5.4: N = {CX}; C0 = {C0}; {CXResult}Результат = {VarResult}";
            DialogResult result = MessageBox.Show(text: $"Записать в буфер обмена [{cntnts}]?", caption: "Вы уверены?",
                buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            { 
                File.AppendAllText(path: "buffer.txt", contents: $"{cntnts}\n", encoding: Encoding.UTF8);
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(text: "Выйти из программы?", caption: "Вы уверены?",
                buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Question);
            e.Cancel = (result == DialogResult.No);
        }

        private void textBoxM_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxCX.Text.Length == 0)|| (textBoxС0.Text.Length == 0))
            {
                buttonCompute.Enabled = false;
            }
            else
            {
                buttonCompute.Enabled = true;
            }
        }

        private void pictureBoxFormula_Click(object sender, EventArgs e)
        {

        }

        private void labelM_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace форм_8._1._2._4
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBoxInput = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCountCX = new System.Windows.Forms.Label();
            this.textBoxС0 = new System.Windows.Forms.TextBox();
            this.textBoxCX = new System.Windows.Forms.TextBox();
            this.buttonCompute = new System.Windows.Forms.Button();
            this.buttonEraseVariables = new System.Windows.Forms.Button();
            this.buttonCommitResult = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.pictureBoxFormula = new System.Windows.Forms.PictureBox();
            this.groupBoxInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxInput
            // 
            this.groupBoxInput.Controls.Add(this.label1);
            this.groupBoxInput.Controls.Add(this.labelCountCX);
            this.groupBoxInput.Controls.Add(this.textBoxС0);
            this.groupBoxInput.Controls.Add(this.textBoxCX);
            this.groupBoxInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxInput.Location = new System.Drawing.Point(13, 120);
            this.groupBoxInput.Name = "groupBoxInput";
            this.groupBoxInput.Size = new System.Drawing.Size(240, 80);
            this.groupBoxInput.TabIndex = 1;
            this.groupBoxInput.TabStop = false;
            this.groupBoxInput.Text = "Переменные";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "C0";
            // 
            // labelCountCX
            // 
            this.labelCountCX.AutoSize = true;
            this.labelCountCX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountCX.Location = new System.Drawing.Point(6, 22);
            this.labelCountCX.Name = "labelCountCX";
            this.labelCountCX.Size = new System.Drawing.Size(79, 16);
            this.labelCountCX.TabIndex = 4;
            this.labelCountCX.Text = "Кол-во cX";
            this.labelCountCX.Click += new System.EventHandler(this.labelM_Click);
            // 
            // textBoxС0
            // 
            this.textBoxС0.Location = new System.Drawing.Point(109, 45);
            this.textBoxС0.Name = "textBoxС0";
            this.textBoxС0.Size = new System.Drawing.Size(125, 22);
            this.textBoxС0.TabIndex = 1;
            this.textBoxС0.TextChanged += new System.EventHandler(this.textBoxM_TextChanged);
            this.textBoxС0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxI_KeyPress);
            // 
            // textBoxCX
            // 
            this.textBoxCX.Location = new System.Drawing.Point(109, 19);
            this.textBoxCX.Name = "textBoxCX";
            this.textBoxCX.Size = new System.Drawing.Size(125, 22);
            this.textBoxCX.TabIndex = 0;
            this.textBoxCX.TextChanged += new System.EventHandler(this.textBoxM_TextChanged);
            this.textBoxCX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM_KeyPress);
            // 
            // buttonCompute
            // 
            this.buttonCompute.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCompute.Location = new System.Drawing.Point(13, 206);
            this.buttonCompute.MaximumSize = new System.Drawing.Size(110, 23);
            this.buttonCompute.MinimumSize = new System.Drawing.Size(110, 23);
            this.buttonCompute.Name = "buttonCompute";
            this.buttonCompute.Size = new System.Drawing.Size(110, 23);
            this.buttonCompute.TabIndex = 2;
            this.buttonCompute.Text = "Рассчитать";
            this.buttonCompute.UseVisualStyleBackColor = true;
            this.buttonCompute.Click += new System.EventHandler(this.buttonCompute_Click);
            // 
            // buttonEraseVariables
            // 
            this.buttonEraseVariables.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEraseVariables.Location = new System.Drawing.Point(143, 206);
            this.buttonEraseVariables.MaximumSize = new System.Drawing.Size(110, 46);
            this.buttonEraseVariables.MinimumSize = new System.Drawing.Size(110, 46);
            this.buttonEraseVariables.Name = "buttonEraseVariables";
            this.buttonEraseVariables.Size = new System.Drawing.Size(110, 46);
            this.buttonEraseVariables.TabIndex = 3;
            this.buttonEraseVariables.Text = "Очистить поля";
            this.buttonEraseVariables.UseVisualStyleBackColor = true;
            this.buttonEraseVariables.Click += new System.EventHandler(this.buttonEraseVariables_Click);
            // 
            // buttonCommitResult
            // 
            this.buttonCommitResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCommitResult.Location = new System.Drawing.Point(259, 206);
            this.buttonCommitResult.MaximumSize = new System.Drawing.Size(110, 46);
            this.buttonCommitResult.MinimumSize = new System.Drawing.Size(110, 46);
            this.buttonCommitResult.Name = "buttonCommitResult";
            this.buttonCommitResult.Size = new System.Drawing.Size(110, 46);
            this.buttonCommitResult.TabIndex = 4;
            this.buttonCommitResult.Text = "Записать результат";
            this.buttonCommitResult.UseVisualStyleBackColor = true;
            this.buttonCommitResult.Click += new System.EventHandler(this.buttonCommitResult_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClose.Location = new System.Drawing.Point(375, 206);
            this.buttonClose.MaximumSize = new System.Drawing.Size(110, 23);
            this.buttonClose.MinimumSize = new System.Drawing.Size(110, 23);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(110, 23);
            this.buttonClose.TabIndex = 5;
            this.buttonClose.Text = "Закрыть";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelResult.Location = new System.Drawing.Point(278, 120);
            this.labelResult.MaximumSize = new System.Drawing.Size(91, 16);
            this.labelResult.MinimumSize = new System.Drawing.Size(91, 16);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(91, 16);
            this.labelResult.TabIndex = 6;
            this.labelResult.Text = "Результат:";
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(281, 142);
            this.textBoxResult.MaximumSize = new System.Drawing.Size(110, 20);
            this.textBoxResult.MinimumSize = new System.Drawing.Size(110, 20);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(110, 20);
            this.textBoxResult.TabIndex = 7;
            // 
            // pictureBoxFormula
            // 
            this.pictureBoxFormula.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFormula.Image")));
            this.pictureBoxFormula.Location = new System.Drawing.Point(13, 13);
            this.pictureBoxFormula.MaximumSize = new System.Drawing.Size(500, 100);
            this.pictureBoxFormula.MinimumSize = new System.Drawing.Size(500, 100);
            this.pictureBoxFormula.Name = "pictureBoxFormula";
            this.pictureBoxFormula.Size = new System.Drawing.Size(500, 100);
            this.pictureBoxFormula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFormula.TabIndex = 0;
            this.pictureBoxFormula.TabStop = false;
            this.pictureBoxFormula.Click += new System.EventHandler(this.pictureBoxFormula_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 261);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonCommitResult);
            this.Controls.Add(this.buttonEraseVariables);
            this.Controls.Add(this.buttonCompute);
            this.Controls.Add(this.groupBoxInput);
            this.Controls.Add(this.pictureBoxFormula);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "MainForm";
            this.Text = "Формула 8.7.1.5.4";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBoxInput.ResumeLayout(false);
            this.groupBoxInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxFormula;
        private System.Windows.Forms.GroupBox groupBoxInput;
        private System.Windows.Forms.TextBox textBoxС0;
        private System.Windows.Forms.TextBox textBoxCX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCountCX;
        private System.Windows.Forms.Button buttonCompute;
        private System.Windows.Forms.Button buttonEraseVariables;
        private System.Windows.Forms.Button buttonCommitResult;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.TextBox textBoxResult;
    }
}


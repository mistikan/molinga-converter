﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace форм_8._1._2._4
{
    public partial class CXForm : Form
    {
        private double C = 0;
        private double X = 0;

        public double ReturnC { get => C; set => C = value; }
        public double ReturnX { get => X; set => X = value; }

        public CXForm()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxC.Text = textBoxC.Text.Replace(oldChar: '.', newChar: ',');
            textBoxX.Text = textBoxX.Text.Replace(oldChar: '.', newChar: ',');
            this.ReturnC = Convert.ToDouble(textBoxC.Text);
            this.ReturnX = Convert.ToDouble(textBoxX.Text);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
